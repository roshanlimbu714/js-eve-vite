import { Navigate, Route, Routes } from "react-router";
import { DashboardLayout } from "../layouts/Dashboard.layout";
import { AuthLayout } from "../layouts/Auth.layout";
import { getToken } from "../service/authentication";

export const AppRoutes = ()=>{
     return <Routes>
        <Route path='/*' element={getToken() ? <DashboardLayout/> : <Navigate to="/auth" />} />
        <Route path='/auth/*' element={ !getToken() ? <AuthLayout/> : <Navigate to="/" />} />
      </Routes>
}