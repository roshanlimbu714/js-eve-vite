import { APIAuthenticateUser } from "../api/authentication";
import baseAxios from "../plugins/axios";


const setAuthorizationHeader = (token) => {
    baseAxios.defaults.headers.common = {
        ...baseAxios.defaults.headers.common,
        Authorization: 'Bearer ' + (token),
    }
}

const deleteAuthorizationHeader = () => {
    delete baseAxios.defaults.headers.common.Authorization
}

const setToken = (data)=>{
    localStorage.setItem('token', data);
}

const setUser = (data)=>{
    localStorage.setItem('user', JSON.stringify(data));
}

export const getToken = (data)=>{
    return localStorage.getItem('token') ?? null;
}

export const getUser = (data)=>{
    return JSON.parse(localStorage.getItem('user') ?? '');
}

export const authenticateUser = async (user)=>{
    const res = await APIAuthenticateUser(user);
    setToken(res.data.token);
    setUser(res.data.user);
    setAuthorizationHeader(res.data.token);
}


export const logout = ()=>{
    localStorage.clear();
    deleteAuthorizationHeader();
}