import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router"

export const TodoDetails =()=>{
    const params = useParams();
    const navigate = useNavigate();
    const [task, setTask] = useState({});
    const [loading, setLoading] = useState(false);
    
    const loadData =async ()=>{
        const res = await fetch('http://localhost:3030/todo/'+ params.id);
        const tasksJson = await res.json();
        setTask(tasksJson.data);
    }

    useEffect(()=>{
        loadData();
    },[])
    return <div>
        <h1>Todo details id: {params.id}</h1>
        <button onClick={()=>navigate(-1)}>Back</button>

        <div className="todo-item" >
                <img src={task.img} alt="" />
                <div className="todo-title">{task.title}</div>
                <div className="todo-status">{task.status}</div>
            </div>
    </div>
}