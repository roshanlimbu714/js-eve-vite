import { useEffect, useState } from "react";
import { useNavigate } from "react-router";

export const Todo = ()=>{
    const [translationY, setTranslationY] = useState(0);
    const navItems = ["Home","About","Services", "Conact","Faq"];
    const [tasks, setTasks] = useState([]);
    const [loading, setLoading] = useState(false);
    
    const loadData =async ()=>{
        const res = await fetch('http://localhost:3030/todo');
        const tasksJson = await res.json();
        setTasks(tasksJson.data);
    }

    const navigate = useNavigate();

    useEffect(()=>{
        loadData();
    },[])
    return <section className="todo-items">
        {tasks.map((v,key)=>(
            <div className="todo-item" key={key} onClick={()=>navigate('/todo/'+v.id)}>
                <img src={v.img} alt="" />
                <div className="todo-title">{v.title}</div>
                <div className="todo-status">{v.status}</div>
            </div>
        ))}
    </section>
}