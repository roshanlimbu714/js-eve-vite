import { useState } from "react"
import { APIAuthenticateUser } from "../api/authentication";
import { APIGetCategories } from "../api/category";
import { authenticateUser } from "../service/authentication";
import { useNavigate } from "react-router";
import { BoxA } from "../components/common/BoxA";
import { BoxE } from "../components/common/BoxE";

export const Login = () => {
    const navigate = useNavigate();
    const [user, setUser] = useState({ username: '', password: '' });
    const [categories, setCategories] = useState([]);

    const loadCategories = () => {
        const res = APIGetCategories();
    }

    const loginUser = async (e) => {
        e.preventDefault();
        await authenticateUser(user);
        window.location.reload();
        navigate('/');
    }

    const inputHandler = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    return <section className="login-screen">
        <form onSubmit={loginUser}>
            <div className="form-group">
                <input type="text" name='username' placeholder='username' onChange={inputHandler} />

            </div>
            <div className="form-group">

                <input type="text" name='password' placeholder='password' onChange={inputHandler} />
            </div>
            <button type="submit">Submit</button>
        </form>
    </section>
}