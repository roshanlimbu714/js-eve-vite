import { useEffect, useState } from "react";

export const SearchMovies =()=>{
    const [movies, setMovies] = useState([]);
    const [loading, setLoading] = useState(false);
    useEffect(()=>{
       loadData();
    },[])

    const loadData =async ()=>{
        setLoading(true);
        const res = await fetch("https://yts.mx/api/v2/list_movies.json");
        const tempMovies = await res.json();
        setMovies(tempMovies.data.movies);
        setLoading(false);
    }
    return  <div className="content movie-content">  {loading? 'loading' :movies.map((movie,key)=>(
        <div key={key} className='movie-card'>
            <img src={movie.medium_cover_image} alt="" />
            <div className="movie-title">{movie.title}</div>
            <div>
            <div className="movie-title">{movie.year}</div>
            <div className="movie-title">{movie.rating}</div>
            </div>
        </div>
       ))}</div>
}