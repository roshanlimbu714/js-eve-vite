export const ServiceHome = () => {
    return <section className="services">
        <div className="text-area">
            <div className="title">Our services</div>
            <div className="sub-title">Our services</div>
        </div>
        <div className="services-area">
            <div className="service-card">
                <div className="service-img">
                    <img src="https://images.pexels.com/photos/19987953/pexels-photo-19987953/free-photo-of-road-in-deep-forest.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load" alt="" />
                </div>
                <div className="service-title">
                    service title
                </div>
                <div className="service-description">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi ad
                </div>
            </div>
            <div className="service-card">
                <div className="service-img">
                    <img src="https://images.pexels.com/photos/19987953/pexels-photo-19987953/free-photo-of-road-in-deep-forest.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load" alt="" />
                </div>
                <div className="service-title">
                    service title
                </div>
                <div className="service-description">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi ad
                </div>
            </div>
            <div className="service-card">
                <div className="service-img">
                    <img src="https://images.pexels.com/photos/19987953/pexels-photo-19987953/free-photo-of-road-in-deep-forest.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load" alt="" />
                </div>
                <div className="service-title">
                    service title
                </div>
                <div className="service-description">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi ad
                </div>
            </div>
        </div>
    </section>
}