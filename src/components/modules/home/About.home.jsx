import { useState } from "react"
import { Dialog } from "../../common/Dialog";

export const AboutHome = () => {

    const [isOpen, setIsOpen]= useState(false);
    return <section className="about">
        <div className="image-area">
            <img src="https://images.pexels.com/photos/19987953/pexels-photo-19987953/free-photo-of-road-in-deep-forest.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load" alt="" />
        </div>
        <div className="text-area">
            <div className="title">title</div>
            <div className="title">this is subtitle</div>
            <div className="description">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi ad exercitationem dolor sed laudantium similique numquam distinctio asperiores placeat totam praesentium eum dicta quia, quasi necessitatibus debitis nemo deserunt ea.
            </div>
            <div className="btn-area"><button onClick={()=>setIsOpen()}>Get Started</button></div>
        </div>
        <Dialog open={isOpen} close={()=>setIsOpen(false)}>
            <h1>Sub title for about</h1>
        </Dialog>
    </section>
}