import { useNavigate } from "react-router"

export const MovieCard = ({ movie }) =>{
    const navigate = useNavigate();

    return <div className='movie-card' onClick={()=>navigate('/movie-details/'+movie.id)}>
    <img src={movie.medium_cover_image} alt="" />
    <div className="movie-title">{movie.title}</div>
    <div>
        <div className="movie-title">{movie.year}</div>
        <div className="movie-title">{movie.rating}</div>
    </div>
</div>
}