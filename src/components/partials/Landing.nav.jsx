import { Link, NavLink, Navigate, useNavigate } from "react-router-dom";
import { logout } from "../../service/authentication";
import {useSelector, useDispatch} from 'react-redux';
import { increaseCounter } from "../../store/modules/count/actions";
export const LandingNav = () => {
    const navigate = useNavigate();
    const navItems = [
        { label:"Home", path:'/'},
        { label:"About", path:'/about'},
        { label:"Services", path:'/todo'},
    ];

    const count = useSelector(state=> state.countReducer.count);

    const dispatch = useDispatch();

    const logoutUser = ()=>{
        logout();
        window.location.reload();
        navigate('/auth');
    }

    return <nav>
        <div className="logo">Logo ({count})</div>
        <div className="nav-items">
            {navItems.map((v, key) => (
                <NavLink className="nav-item" to={v.path} key={key}>{v.label}</NavLink>
            ))}

            <button onClick={logoutUser}>Logout</button>
            <button onClick={()=> dispatch(increaseCounter())}>Inc Count</button>
        </div>
    </nav>
}