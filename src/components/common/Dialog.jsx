export const Dialog = (props) =>{
    return props.open ? <div className="dialog">
    <div className="backdrop" onClick={props.close}></div>
    <div className="dialog-content">
        {props.children}
    </div> 
</div>: ''
}